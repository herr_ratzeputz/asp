import thread
import socket
import time
import os
import clingo

class KiwaSocket(object):

    def __init__(self):
        self._host = "127.0.0.1"            #init default host and port
        self._port = 5000

        self._s = None

        self._solverThread = None
        self._model = None

    def __del__(self):
        self._s.close()

    def setModel(self, model):
        self._model = model

    def runSolver(self, command):
        self.close()
        self._solverThread = thread.start_new_thread(lambda: os.system(command), ())
    
    def is_connected(self):
        return self._s is not None

    def close(self):
        if self._s != None: self._s.close()
        self._s = None
        
    def connect(self, host = None, port = None):
        if self.is_connected() and host == self._host and port == self._port:
            return 0
        if host is not None:
            self._host = host
        if port is not None:
            self._port = port
        print "Connect with server"
        if self._s is not None:
            self._s.close()
        self._s = socket.socket()
        connected = False
        tryCount = 0

        while not connected:            #try to connect to the server
            try:
                self._s.connect((self._host, self._port))
                connected = True
            except(socket.error):
                if tryCount >= 6: 
                    print "Failed to connect with server"
                    self._s = None
                    return -1 
                print "Failed to connect with server\nRetrying in 5 sek"
                time.sleep(5)
            tryCount += 1
        return 0

    def solve(self):
        if self._s == None or self._model == None: return -1
        # send file to instance
        self._s.send(self._model.getFilePath())

        # get resulting models
        atoms = []
        data = ""
        while data.find('\n') == -1:
            data += self._s.recv(2048)

        sAtoms = data.split('.')
        for sAtom in sAtoms: 
            if len(sAtom) != 0 and not (len(sAtom) == 1 and sAtom[0] == '\n'):
                atoms.append(clingo.parse_term(sAtom))
        for atom in atoms:
            self._model.onAtom(atom)
        self._model.update_windows()
