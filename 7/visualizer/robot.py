import visualizer
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from clingo import *

class Robot(QGraphicsRectItem):
    def __init__(self):
        super(self.__class__, self).__init__()
        self._ID = None
        self._firstX = None
        self._firstY = None
        self._x  = 1
        self._y  = 1
        self._carries = None
        self._actions = []
        self._dragged = False
        self._enable_drag = False
        self.setAcceptedMouseButtons(Qt.MouseButtons(1))

    def mousePressEvent(self, event):
        if self._enable_drag:
            self._dragged = True

    def mouseReleaseEvent(self, event):
        if self._dragged == True:
            self._dragged = False
            for view in self.scene().views():
                if isinstance(view, visualizer.Visualizer):
                    view.moveItem(self, self.rect().topLeft())

    def mouseMoveEvent(self, event):
        if self._dragged == True:
            rect = self.rect()
            rect.moveTo(event.pos().x(), event.pos().y())
            self.setRect(rect)

    def enable_drag(self, enable):
        self._enable_drag = enable

    def doAction(self, timeStep):
        if timeStep >= len(self._actions):  return 0  #break, if no action is defined
        if self._actions[timeStep] == None: return 0  #break, if no action is defined
        if self._actions[timeStep].name == "move":
            if len(self._actions[timeStep].arguments) != 2: return -1
            try:
                moveX = self._actions[timeStep].arguments[0].number
                moveY = self._actions[timeStep].arguments[1].number
                self._x = self._x + moveX
                self._y = self._y + moveY
            except:
                self._x = self._x
                self._y = self._y
            if self._carries != None: self._carries.set_position(self._x, self._y)
            return 1
        elif self._actions[timeStep].name == "pickup":
            return 2
        elif self._actions[timeStep].name == "putdown":
            if self._carries == None:
                return -3
            self.setCarries(None)
            return 3
        elif self._actions[timeStep].name == "deliver":
            return 4
        return 0

    def undoAction(self, timeStep):
        if timeStep < 0 or timeStep >= len(self._actions): return 0
        if self._actions[timeStep] == None: return 0
        if self._actions[timeStep].name == "move":
            if len(self._actions[timeStep].arguments) != 2: return -1
            try:
                moveX = self._actions[timeStep].arguments[0].number
                moveY = self._actions[timeStep].arguments[1].number
                self._x = self._x - moveX
                self._y = self._y - moveY
            except:
                self._x = self._x
                self._y = self._y
            if self._carries != None: self._carries.set_position(self._x, self._y)
            return 1
        elif self._actions[timeStep].name == "pickup":
            if self._carries == None: return -3
            else: 
                self.setCarries(None)
                return 3
        elif self._actions[timeStep].name == "putdown": return 2
        elif self._actions[timeStep].name == "deliver":
            return 4
        return 0

    def to_fun_str(self):
        s = ("init(object(robot," 
            + str(self._ID) + "),value(at,(" 
            + str(self._firstX) + "," 
            + str(self._firstY) + "))).")
        return s

    def clearActions(self):
        self._actions = []

    def set_id(self, robotID):
        self._ID = robotID

    def set_position(self, x, y):
        self._x = x
        self._y = y

    def set_starting_position(self, x, y):
        if self._firstX == None: self._x = x
        if self._firstY == None: self._y = y
        self._firstX = x
        self._firstY = y

    def setAction(self, action, timeStep):
        if timeStep < 0:
            print "Warning: invalid time step in action: do(" + str(self._ID) + "," + str(action) + "," + str(timeStep) + ")"
            print "time step is less than 0"
            return
        for ii in range((timeStep + 1) - len(self._actions)):
            self._actions.append(None)
        if not self._actions[timeStep] == None:
            print "Warning: for robot(" + str(self._ID) + ") multiple actions are defined at time step: " + str(timeStep)
        self._actions[timeStep] = action

    def setCarries(self, shelf):
        if shelf == self._carries: return
        old = self._carries
        self._carries = shelf
        if old != None: old.setCarried(None)
        if self._carries != None: self._carries.setCarried(self)

    def restart(self):
        self._x = self._firstX
        self._y = self._firstY 
        self.setCarries(None)

    def set_color(self, color):
        brush = QBrush(color)
        self.setBrush(brush)

    def get_id(self):
        return self._ID

    def get_positionx(self):
        return self._x

    def get_positiony(self):
        return self._y

    def getStartingPositionX(self):
        return self._firstX

    def getStartingPositionY(self):
        return self._firstY

    def getAction(self, timeStep):
        if timeStep >= len(self._actions): return Function("idle", [])
        action = self._actions[timeStep]
        if action == None: action = Function("idle", [])
        return action

    def get_actions(self):
        return self._actions

    def getCarries(self):
        return self._carries 

    def get_actions_fun(self):
        actions = []   
        count = 0     
        for action in self._actions:
            if action is None:
                actions.append(None)
            else:
                str_action = "occurs(object(robot, " + str(self._ID) + "), action(" + str(action.name) + ", ("
                length = len(action.arguments)
                num = 1
                for arg in action.arguments:
                    str_action = str_action + str(arg)
                    if num < length:
                        str_action = str_action + ", " 
                    num = num + 1
                str_action = str_action + ")), " + str(count) +  ")"
                actions.append(str_action)
            count = count + 1 
        return actions

    def get_deliver(self, time_step):
        if time_step >= len(self._actions):  
            return None
        elif self._actions[time_step].name == "deliver":
            return self._actions[time_step].arguments
        else:
            return None

