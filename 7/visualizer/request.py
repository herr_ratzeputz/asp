class Request(object):

    def __init__(self, item_id = None, station_id = None, station = None, requested_amount = 1):
        self._id = None
        self._item_id = item_id
        self._station_id = station_id
        self._station = station
        self._requested_amount = requested_amount  
        self._received_amount = 0
        self._shelves = []
        self._fulfilled = False
        self._time_step = 0

    def __str__(self):
        return self.to_fun_str()
    
    def set_id(self, request_id):
        self._id = request_id

    def set_item_id(self, item_id):
        self._item_id = item_id

    def set_station_id(self, station_id):
        self._station_id = station_id

    def set_station(self, station):
        self._station = station

    def set_requested_amount(self, amount):
        self._requested_amount = amount

    def set_time_step(self, timeStep):
        self._time_step = timeStep

    def set_fulfilled(self, fulfilled):
        if not fulfilled: self._received_amount  = 0
        self._fulfilled = fulfilled

    def add_shelf(self, shelf):
        self._shelves.append(shelf)

    def deliver(self, delivered):
        self._received_amount = self._received_amount + delivered

    def is_fulfilled(self):      
        if self._received_amount == self._requested_amount:
            self._fulfilled = True
        return self._fulfilled

    def to_fun_str(self):
        return ("init(object(order," + str(self._id) 
                + "),value(line,(" + str(self._item_id) 
                + ","+ str(self._requested_amount) + ")))." 
                +"init(object(order," + str(self._id) 
                + "),value(pickingStation," + str(self._station_id) 
                + ")).")

    def get_id(self):
        return self._id

    def get_item_id(self):
        return self._item_id

    def get_station_id(self):
        return self._station_id

    def get_station(self):
        return self._station

    def get_requested_amount(self):
        return self._requested_amount

    def get_received_amount(self):
        return self._received_amount

    def get_time_step(self):
        return self._time_step

    def get_fulfilled(self):
        return self._fulfilled

    def get_shelves(self):
        return self._shelves
