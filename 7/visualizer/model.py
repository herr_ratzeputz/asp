from robot import *
from shelf import *
from pickingStation import *
from request import *
import clingo

class Model(object):
    def __init__(self):
        self._file_path = ""
        self._windows = []
        self._delivered = [""]

        self._robots    = []    #robots
        self._shelves  = []    #shelves
        self._stations = []  #picking stations
        self._requests = []    #requests
        self._items     = []  #list of all items

        self._msg = []                #messages
        
        self._grid_size_x  = 1         #size of the grid (x-direction)
        self._grid_size_y  = 1         #size of the grid (y-direction)
        self._num_steps = 0
        self._timeStep  = 0
        self._maxTimeStep = 0
        self._goal = False
        self._enable_drag = True

        self._inits = []
        self._nodes = []                #pairs of x and y
        self._blocked_nodes = []        #same here
        self._highways = []             #same here

    def clear(self):
        self._file_path = ""
        self._delivered = [""]
        self._robots    = []    #robots
        self._shelves  = []    #shelves
        self._stations = []  #picking stations
        self._requests = []    #requests
        self._items     = []  #list of all items

        self._msg = []                #messages
        
        self._grid_size_x  = 1         #size of the grid (x-direction)
        self._grid_size_y  = 1         #size of the grid (y-direction)
        self._num_steps = 0
        self._timeStep  = 0
        self._maxTimeStep = 0
        self._goal = False
        self._enable_drag = True

        self._inits = []
        self._nodes = []                #pairs of x and y
        self._blocked_nodes = []        #same here
        self._highways = []             #same here

    def get_delivered(self, time_step):
        if time_step >= len(self._delivered): 
            return ""
        return self._delivered[time_step]

    def update(self):
        if self._timeStep > self._num_steps or self._num_steps == 0: return self._timeStep
        if self._timeStep >= len(self._delivered):
            self._delivered.append("")
        if self._timeStep == 0: 
            self._delivered[self._timeStep] = ""
        else: 
            self._delivered[self._timeStep] = self._delivered[self._timeStep-1]
        #do robot actions
        for robot in self._robots:
            result = robot.doAction(self._timeStep)
            if result == 2:            #action == pickup
                for shelf in self._shelves:
                    if shelf.get_positionx() == robot.get_positionx() and shelf.get_positiony() == robot.get_positiony():
                        robot.setCarries(shelf)
                        break   
            elif result == 4:         #action == deliver   
                deliver = robot.get_deliver(self._timeStep)
                shelf = robot.getCarries()
                if shelf is not None: 
                    shelf.removeItem(deliver[1], deliver[2].number)
                self._delivered[self._timeStep] = self._delivered[self._timeStep] + ("delivered " + str(deliver[2]) + " units of product " +
                                          str(deliver[1]) + " for order " + str(deliver[0]) + " at timestep " + str(self._timeStep) + "\n")  
                request = self.getRequest(deliver[0], deliver[1])
                if request is not None:
                    request.deliver(deliver[2].number)

        #update requests
        goal = True
        if len(self._requests) == 0: goal = False
        for request in self._requests:
            if not request.get_fulfilled() and self._timeStep >= request.get_time_step():
                if not request.is_fulfilled(): 
                    goal = False
                #else:
                #    self._deliver_log.append("request "+ str(request.get_id()) " is fulfilled at time step: " + str(self._timeStep))
        if goal and not self._goal:
            self._delivered[self._timeStep] = self._delivered[self._timeStep] + ("all orders are fulfilled at time step: " + str(self._timeStep))
            self._goal = True

        #check if all requests are fulfilled
        if self._timeStep == self._num_steps and not self._goal:
            for request in self._requests:
                if not request.get_fulfilled():
                    msg = ("Error: the last time step is reached,\n    but request(" + str(request.get_id()) + ", " + 
                        str(request.get_item_id()) + ", " + str(request.get_station_id()) + ", " + str(request.get_requested_amount()) +
                        ") is not fulfilled")
                    self._msg.append(msg)
        self._timeStep += 1
        if self._timeStep > self._maxTimeStep: self._maxTimeStep = self._timeStep 
        self.update_windows()
        return self._timeStep

    def load_answer(self, fileName):
        control = clingo.Control()
        control.load(fileName)
        control.ground([("base", [])])
        control.solve(self.on_model, [])

    def on_model(self, model):
        for atom in model.symbols(atoms=True):
            self.onAtom(atom)

    def update_windows(self):
        for window in self._windows:
            window.update()

    def onAtom(self, atom):
        if atom.name == "occurs" and len(atom.arguments) == 3:
            try:
                obj = atom.arguments[0]
                action = atom.arguments[1]
                time_step = atom.arguments[2]
                
                if obj.arguments[0].name == "robot" and obj.name == "object" and action.name == "action":
                    robotid = obj.arguments[1]
                    robot = self.getRobot2(robotid)
                    robot.setAction(clingo.Function(action.arguments[0].name, action.arguments[1].arguments), time_step.number)
                    if time_step.number > self.getNumSteps(): self.setNumSteps(time_step.number)
            except:
                print "invalid occurs format, expecting: occurs(object([object], [objectID]), action([action], [arguments]), [time step])"


    def isValid(self):        #check whether the current time step contains invalid actions
        result = True
        if self._maxTimeStep > self._timeStep: return result
        for robot in self._robots:
            action = robot.getAction(self._timeStep)
            if action.name() == "move":            #action == move
                if len(action.args()) != 2: 
                    self._msg.append("invalid move format, \n    expecting: move(X, Y)")
                else:
                    try:
                        destinationX = robot.get_positionx() + action.args()[0]
                        destinationY = robot.get_positiony() + action.args()[1]
                        if (destinationX > self._grid_size_x or destinationX < 1 or    #move out of the grid
                            destinationY > self._grid_size_y or destinationY < 1):
                            result = False
                            error = ("Error at time step: " + str(self._timeStep) + 
                                    "\n    robot(" + str(robot.get_id()) + ") moves out of the grid\n    from: (" + 
                                    str(robot.get_positionx()) + ", " + str(robot.get_positiony()) + ") to: (" + 
                                    str(destinationX) + ", " + str(destinationY) + ")")
                            self._msg.append(error)

                            value = (int)(math.fabs(action.args()[0]) + math.fabs(action.args()[1]))
                            if value > 1 :                        #move more then one field
                                result = False
                                error = ("Error at time step: " + str(self._timeStep) + 
                                        "\n    robot(" + str(robot.get_id()) + ") moves more than one \n    field in one time step\n    from: (" + 
                                        str(robot.get_positionx()) + ", " + str(robot.get_positiony()) + ") to: (" + 
                                        str(destinationX) + ", " + str(destinationY) + ")")
                                self._msg.append(error)

                    except:
                        self._msg.append("invalid move format, \n    expecting: move(X, Y)")    

            if action.name() == "pickup":            #action == lift
                shelf = None
                for shelf2 in self._shelves:
                    if shelf2.get_positionx() == robot.get_positionx() and shelf2.get_positiony() == robot.get_positiony():
                        shelf = shelf2
                        break              
                if shelf == None:                #no shelf is at the same position as the robot
                    error = ("Error at time step: " + str(self._timeStep) +
                        "\n    robot(" + str(robot.get_id()) + ") lifts without a shelf \n    on position: (" + 
                        str(robot.get_positionx()) + ", " + str(robot.get_positiony()) + ")")
                    result = False
                    self._msg.append(error)

            if action.name() == "putdown":            #action == place
                if robot.getCarries() == None:        #the robot carries no shelf
                    error = ("Error at time step: " + str(self._timeStep) + 
                        "\n    robot(" + str(robot.get_id()) + ") placed a shelf \n    without carrying a shelf")
                    result = False
                    self._msg.append(error)
 
            #check, whether two robots collide
            for robot2 in self._robots:
                if robot2 == robot: break
                action2 = robot2.getAction(self._timeStep)
                destination1 = (0,0)
                destination2 = (0,0)

                if action.name() == "move": 
                    try:     destination1 = (robot.get_positionx() + action.args()[0], robot.get_positiony() + action.args()[1])
                    except: destination1 = (robot.get_positionx(), robot.get_positiony())
                else:        destination1 = (robot.get_positionx(), robot.get_positiony())

                if action2.name() == "move": 
                    try:     destination2 = (robot2.get_positionx() + action2.args()[0], robot2.get_positiony() + action2.args()[1])
                    except: destination2 = (robot2.get_positionx(), robot2.get_positiony())
                else:        destination2 = (robot2.get_positionx(), robot2.get_positiony())

                if destination1 == destination2:            #two robots collide
                    error = ("Error at time step: " + str(self._timeStep) + 
                        "\n    robot(" + str(robot.get_id()) + ") and robot(" + str(robot2.get_id()) + ") collide " + 
                        "\n    at position (" + str(destination1[0]) + ", " + str(destination1[1]) + ")")
                    self._msg.append(error)
                    result = False
        
        #check, whether two shelves collide
        for shelf in self._shelves:

            destination1 = (0,0)
            robot = shelf.getCarried()
            if robot == None: destination1 = (shelf.get_positionx(), shelf.get_positiony())
            else:
                action = robot.getAction(self._timeStep)
                if action.name() == "move": 
                    try:     destination1 = (robot.get_positionx() + action.args()[0], robot.get_positiony() + action.args()[1])
                    except: destination1 = (robot.get_positionx(), robot.get_positiony())
                else:        destination1 = (robot.get_positionx(), robot.get_positiony())

            for shelf2 in self._shelves:
                if shelf == shelf2: break

                destination2 = (0,0)
                robot = shelf2.getCarried()
                if robot == None: destination2 = (shelf2.get_positionx(), shelf2.get_positiony())
                else:
                    action = robot.getAction(self._timeStep)
                    if action.name() == "move": 
                        try:     destination2 = (robot.get_positionx() + action.args()[0], robot.get_positiony() + action.args()[1])
                        except: destination2 = (robot.get_positionx(), robot.get_positiony())
                    else:        destination2 = (robot.get_positionx(), robot.get_positiony())

                if destination1 == destination2:            #two shelves collide
                    error = ("Error at time step: " + str(self._timeStep) + 
                        "\n    shelf(" + str(shelf.get_id()) + ") and shelf(" + str(shelf2.get_id()) + ") collide" + 
                        "\n    at position (" + str(shelf.get_positionx()) + ", " + str(shelf.get_positiony()) + ")")
                    result = False
                    self._msg.append(error)
        return result

    def addInit(self, init):
        self._inits.append(str(init) + '.')

    def undo(self):
        if self._timeStep == 0: return self._timeStep
        self._timeStep -= 1
        #do robot actions
        for robot in self._robots:
            result = robot.undoAction(self._timeStep)
            if result == 2:            #action == putdown
                for shelf in self._shelves:
                    if shelf.get_positionx() == robot.get_positionx() and shelf.get_positiony() == robot.get_positiony():
                        robot.setCarries(shelf)
                        break  
            elif result == 4:         #action == deliver   
                deliver = robot.get_deliver(self._timeStep) 
                shelf = robot.getCarries()
                if shelf is not None: 
                    shelf.put_back_item(deliver[1], deliver[2].number)
                request = self.getRequest(deliver[0], deliver[1])
                if request is not None:
                    request.deliver(-deliver[2].number)
        self.update_windows()
        return self._timeStep

    def clearActions(self):
        for robot in self._robots:
            robot.clearActions()

    def restart(self):                #set all robots and shelfs to the starting positions
        for robot in self._robots:
            robot.restart()
        for shelf in self._shelves:
            shelf.restart()
        for request in self._requests:
            request.set_fulfilled(False)
        self._goal = False
        self._timeStep  = 0

    def countItems(self):
        self._items = []
        for shelf in self._shelves:
            for item in shelf.getItems(): 
                isInList = False
                for item2 in self._items:
                    if item[0] == item2[0]:
                        if item2[1] != 0:
                            self._items.append((item2[0], item2[1] + item[1], item2[2]))
                            self._items.remove(item2)
                            isInList = True
                            break
                if not isInList: self._items.append((item[0], item[1], 0))
        for request in self._requests:
            item = request.get_item_id()
            amount = request.get_requested_amount()
            for item2 in self._items:      
               if item2[0] == item:
                   self._items.append((item2[0], item2[1], item2[2] + amount))
                   self._items.remove(item2)

    def updateRequests(self):
        #find shelves for requests
        for request in self._requests:
            for shelf in self._shelves:
                if shelf.contains(request.get_item_id()): request.add_shelf(shelf)
            station = self.get_station(request.get_station_id())        #find picking stations for requests
            if station == None: 
                self._msg.append("a request can not be fulfilled: \n    picking station " + str(request.get_station_id()) + 
                    " does not exist")
            else: request.set_station(station)
            if len(request.get_shelves()) == 0:
                self._msg.append("a request can not be fulfilled: \n    no shelf contains item " + str(request.get_item_id()))

    def set_deliver_log(self, deliver_log):
        self._deliver_log = deliver_log

    def getRobotInNode(self, x, y):
        for robot in self._robots:
            if robot.get_positionx() == x and robot.get_positiony() == y:
                return robot

    def getShelfInNode(self, x, y):
        for shelf in self._shelves:
            if shelf.get_positionx() == x and shelf.get_positiony() == y:
                return shelf

    def getPickingStationInNode(self, x, y):
        for station in self._stations:
            if station.get_positionx() == x and station.get_positiony() == y:
                return station

    def getMsg(self):
        temp = self._msg
        self._msg = []
        return temp

    def getRequest(self, requestID, requested_item):
        for request in self._requests:
            if (str(request.get_id()) == str(requestID)) and str(request.get_item_id()) == str(requested_item):
                return request
        return None

    def getRobot(self, robotID):
        for robot in self._robots:
            if str(robot.get_id()) == str(robotID): return robot
        return None 

    def getRobot2(self, robotID):
        for robot in self._robots:
            if robot.get_id() == None:
                robot.set_id(robotID)
                return robot 
            elif str(robot.get_id()) == str(robotID): return robot
        self._robots.append(Robot())
        robot = self._robots[len(self._robots) - 1]
        self._num_robots = len(self._robots)
        robot.set_id(robotID)
        robot.enable_drag(self._enable_drag)
        return robot
    
    def createRobotAt(self, x, y):
        ID = 1
        foundFreeID = False
        while not foundFreeID:
            foundFreeID = True
            for robot in self._robots:
                if str(robot.get_id()) == str(ID): 
                    ID = ID + 1
                    foundFreeID = False
                    break
        robot2 = Robot()
        robot2.set_starting_position(x, y)
        robot2.set_id(ID)
        self._robots.append(robot2)
        robot2.enable_drag(self._enable_drag)

    def removeRobot(self, robot):
        self._robots.remove(robot)

    def getRobots(self):
        return self._robots

    def getShelf(self, shelfID):
        for shelf in self._shelves:
            if str(shelf.get_id()) == str(shelfID): return shelf
        return None 

    def getShelf2(self, shelfID):
        for shelf in self._shelves:
            if shelf.get_id() == None:
                shelf.set_id(shelfID)
                return shelf 
            elif str(shelf.get_id()) == str(shelfID): return shelf
        self._shelves.append(Shelf())
        shelf = self._shelves[len(self._shelves) - 1]
        self._num_shelves = len(self._shelves)
        shelf.set_id(shelfID)
        shelf.enable_drag(self._enable_drag)
        return shelf

    def createShelfAt(self, x, y):
        ID = 1
        foundFreeID = False
        while not foundFreeID:
            foundFreeID = True
            for shelf in self._shelves:
                if str(shelf.get_id()) == str(ID): 
                    ID = ID + 1
                    foundFreeID = False
                    break
        shelf2 = Shelf()
        shelf2.set_starting_position(x, y)
        shelf2.set_id(ID)
        self._shelves.append(shelf2)
        shelf2.enable_drag(self._enable_drag)

    def removeShelf(self, shelf):
        self._shelves.remove(shelf)

    def get_shelves(self):
        return self._shelves

    def get_station(self, pickingStationID):
        for station in self._stations:
            if str(station.get_id()) == str(pickingStationID): return station
        return None 

    def get_station2(self, pickingStationID):
        for station in self._stations:
            if station.get_id() == None:
                station.set_id(pickingStationID)
                return station 
            elif str(station.get_id()) == str(pickingStationID): return station
        self._stations.append(PickingStation())
        station = self._stations[len(self._stations) - 1]
        self._num_stations = len(self._stations)
        station.set_id(pickingStationID)
        return station

    def createPickingStationAt(self, x, y):
        ID = 1
        foundFreeID = False
        while not foundFreeID:
            foundFreeID = True
            for station in self._stations:
                if str(station.get_id()) == str(ID): 
                    ID = ID + 1
                    foundFreeID = False
                    break
        station2 = PickingStation()
        station2.set_starting_position(x, y)
        station2.set_id(ID)
        self._stations.append(station2)
        station2.enable_drag(self._enable_drag)

    def removePickingStation(self, station):
        self._stations.remove(station)

    def get_stations(self):
        return self._stations

    def setNumSteps(self, num):
        self._num_steps = num

    def setGridSize(self, X, Y):
        if X < 1:
            X = 1
        if Y < 1:
            Y = 1 

        to_remove = []
        for node in self._nodes:        #remove old nodes
            if node[0] > X: 
                to_remove.append(node)
            elif node[1] > Y:
                to_remove.append(node)
        for node in to_remove:
            self._nodes.remove(node)  

        to_remove = []
        for node in self._blocked_nodes:        #remove old nodes
            if node[0] > X: 
                to_remove.append(node)
            elif node[1] > Y:
                to_remove.append(node)
        for node in to_remove:
            self._blocked_nodes.remove(node)  

        self._blocked_nodes = []
        for x in range(1, X+1):
            for y in range(1, Y+1):
                self._blocked_nodes.append((x,y))
        for node in self._nodes:
            self._blocked_nodes.remove(node)

        self._grid_size_x = X
        self._grid_size_y = Y

    def add_node(self, x, y):
        if (x,y) in self._nodes:
            return

        self._nodes.append((x, y))
        if (x,y) in self._blocked_nodes:
            self._blocked_nodes.remove((x,y))
        if x > self._grid_size_x or y > self._grid_size_y:
            self.setGridSize(max(x, self._grid_size_x), max(y, self._grid_size_y))

    def is_node(self, x, y):
        return (x, y) in self._nodes

    def remove_node(self, x, y):
        if (x,y) not in self._nodes:
            return

        self._nodes.remove((x, y))
        if (x,y) not in self._blocked_nodes:
            self._blocked_nodes.append((x,y))

    def blocked_nodes(self):
        return self._blocked_nodes

    def addRequest(self, request):
        self._requests.append(request)
        self._goal = False
        itemID = request.get_item_id()
        amount = request.get_requested_amount()
        for item in self._items:      
            if str(item[0]) == str(itemID):
                self._items.append((item[0], item[1], item[2] + amount))
                self._items.remove(item)

    def remove_order(self, order):
        if order in self._requests:
            self._requests.remove(order)

    def saveToFile(self, fileName):
        ofile = open(fileName, "w")
        try:
            #head
            ofile.write("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            ofile.write("\n%Grid Size X: " + str(self._grid_size_x))
            ofile.write("\n%Grid Size Y: " + str(self._grid_size_y))
            ofile.write("\n%Number of robots: " + str(len(self._robots)))
            ofile.write("\n%Number of shelves: " + str(len(self._shelves)))
            ofile.write("\n%Number of picking stations: " + str(len(self._stations)))
            ofile.write("\n%Number of product kinds: " + str(len(self._items)))
            ofile.write("\n%Number of orders: " + str(len(self._requests)))
            ofile.write("\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")    
            #body
            ofile.write("#program base.\n\n")
            ofile.write("timelimit("+ str(int(self._grid_size_x * self._grid_size_y * 1.5)) + ").\n\n")

            ofile.write("%init\n")                                                                  #inits
            for obj in self.getFunStrings():
                 ofile.write(str(obj) + "\n")
    
        except IOError:
            ofile.close()
            return

        ofile.close()    
        return

    def save_answer(self, file_name):
        ofile = open(file_name, "w")
        try:
            for robot in self._robots:
                for action in robot.get_actions_fun():
                    if action is not None:
                        ofile.write(action + ".\n")

        except IOError:
            ofile.close()
            return

        ofile.close()    
        return            

    def enable_drag(self, enable):
        self._enable_drag = enable
        for robot in self._robots:
            robot.enable_drag(enable)
        for shelf in self._shelves:
            shelf.enable_drag(enable)
        for station in self._stations:
            station.enable_drag(enable)

    def is_drag_enabled(self):
        return self._enable_drag

    def is_highway(self, x, y):
        return (x,y) in self._highways

    def add_highway(self, x, y):
        self._highways.append((x,y))

    def remove_highway(self, x, y):
        if (x,y) in self._highways:
            self._highways.remove((x,y))

    def getFunStrings(self):
        s = []
        for node in self._nodes:
            s.append("init(object(node, " + str(node[0] + (node[1]-1) * self._grid_size_x) +  ") , value(at, (" + str(node[0]) + ", " + str(node[1]) + "))).")
        for node in self._highways:
            s.append("init(object(highway, " + str(node[0] + (node[1]-1) * self._grid_size_x) +  ") , value(at, (" + str(node[0]) + ", " + str(node[1]) + "))).")
        for robot in self._robots:
            s.append(robot.to_fun_str())
        for shelf in self._shelves:
            s.append(shelf.to_fun_str())
        for station in self._stations:
            s.append(station.to_fun_str())
        for request in self._requests:
            s.append(request.to_fun_str())
        for node in self._inits:
            s.append(node)
        return s    

    # return the file path for sending to the solver
    def getFilePath(self):
        return self._file_path
    
    def getRequests(self):
        return self._requests

    def getGridSizeX(self):
        return self._grid_size_x

    def getGridSizeY(self):
        return self._grid_size_y

    def getNumRobots(self):
        return len(self._robots)

    def getNumShelves(self):
        return len(self._shelves)

    def getNumStations(self):
        return len(self._stations)

    def getNumSteps(self):
        return self._num_steps

    def getCurrentTimeStep(self):
        return self._timeStep

    def getMaxTimeStep(self):
        return self._maxTimeStep

    def getItems(self):
        return self._items

    def get_highways(self):
        return self._highways

    def add_window(self, window):
        self._windows.append(window)
