import argparse
import time
import os.path
import model
import clingo
from request import *
import socket

VERSION = "0.1.4"

class Solver(object):
    def __init__(self):

        self._parser = argparse.ArgumentParser()
        self._parser.add_argument("-p", "--port", help="the port the solver will send the anwsers to",
                                            type=int, default = 5000)
        self._parser.add_argument("-v", "--version",
                                            help="show the current version", action="version",
                                            version=VERSION)
        self._args = self._parser.parse_args()

        self._solution = False
        self._control = None
        self._atoms =[]

        self._host = "127.0.0.1"
        self._port = self._args.port
        self._socket = None
        self._connection = None

    def on_model(self, m):
        print "found solution"
        self._atoms =[]
        for x in m.symbols(atoms=True):
            self._atoms.append(x)
        return True

    def instances(self, m):
        print "get instances"
        self._instances = 1
        for x in m.symbols(atoms=True):
            if x.name == "instances":
                self._instances = int(str(x.arguments[0]))

    def within_borders(self):
        return self._num_steps <= 50 * self._instance_num and self._instance_num <= self._instances
                
    def solveInc(self):                                            #solve incremental
        try:
            self._control.load("./encodings/encoding.lp")            #load encoding
            #grind and solve            
            result = clingo.SolveResult.unsatisfiable
            self._num_steps = 1
            self._instance_num = 1
            
            # ground the base parts
            #print "ground..."                
            time_total_start = time.clock()
            self._control.ground([("base", []), ("init", []), ("step", [self._num_steps]), ("check", [self._num_steps])])
            #print "time for grounding: ", time.clock() - time_total_start, " sec"
            

            while self.within_borders(): 
                time_solving_start = time.clock()  
                #print "timestep: ", self._num_steps
                self._control.ground([("step", [self._num_steps]), ("check", [self._num_steps])])
                #print "time for grounding: ", time.clock() - time_solving_start, " sec"

                self._control.assign_external(clingo.Function("query", [self._num_steps]), True) 
                #print "solve..."
                #time_solving_start = time.clock()
                result = self._control.solve(self.on_model, [])    

                if str(result) == "UNSAT":
                    result = clingo.SolveResult.unsatisfiable
                elif str(result) == "SAT":
                    self._instance_num += 1
                    
                #print "time for solving: ", time.clock() - time_solving_start, " sec"  
                print "time for step: ", time.clock() - time_solving_start, " sec"  
                self._control.assign_external(clingo.Function("query", [self._num_steps]), False) 
                self._num_steps += 1 

            print "total time: ", time.clock() - time_total_start    
        except RuntimeError as error:
             print error
        return

    def run(self):
        print "Start server"

        try:
            self._socket = socket.socket()
            self._socket.bind((self._host, self._port))
            self._socket.listen(1)
            self._connection, addr = self._socket.accept()
            print "Connection with: " + str(addr)
            while(True):
                self._control = clingo.Control()
                # get instance path
                path = self._connection.recv(2048)
                if path == '':
                    print "Close server"
                    if self._connection is not None: self._connection.close()
                    if self._socket is not None: self._socket.close()
                    return

                self._control.load(path)
                self._control.ground([("base", [])])
                self._control.solve(self.instances, [])
                
                self.solveInc()

                # send result
                for atom in self._atoms:
                    self._connection.send(str(atom) + '.')
                self._connection.send('\n')

        except socket.error as error:
            if self._connection is not None: self._connection.close()
            if self._socket is not None: self._socket.close()
            print error
            print "Close server because of errors"
            return         

        self._connection.close()
        self._socket.close()
        print "Close server"
        return

solver = Solver()
solver.run()
