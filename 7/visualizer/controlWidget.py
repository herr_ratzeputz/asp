from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

class ControlWidget(QWidget):
    def __init__(self):
        super(self.__class__, self).__init__()    

        self._visualizer = None
        self._timestep_widget = QTextEdit(self)
        self._timestep_widget.setReadOnly(True)
        self._timestep_widget.resize(self.size().width(), 40)
        self._timestep_widget.setFontPointSize(18)          
  
        self._update_button = QPushButton('>', self)
        self._update_button.move(100,50)
        self._update_button.resize(30,30)
        self._update_button.clicked.connect(self.on_update)
 
        self._pause_button = QPushButton('||', self)
        self._pause_button.move(60,50)
        self._pause_button.resize(30,30)
        self._pause_button.clicked.connect(self.on_pause)

        self._undo_button = QPushButton('<', self)
        self._undo_button.move(20,50)
        self._undo_button.resize(30,30)
        self._undo_button.clicked.connect(self.on_undo)

    def set_visualizer(self, visualizer):
        self._visualizer = visualizer
        self._visualizer.getModel().add_window(self)
        self.update()

    def on_update(self, event):
        if self._visualizer is not None:
            self._visualizer.stop_timer()
            self._visualizer.updateModel(True)

    def on_pause(self, event):
        if self._visualizer is not None:
            self._visualizer.switch_timer()
    
    def on_undo(self, event):
        if self._visualizer is not None:
            self._visualizer.stop_timer()
            self._visualizer.undoModel()

    def update(self):
        if self._visualizer is None: 
            return
        if self._visualizer.getModel() is None:
            return
        step = self._visualizer.getModel().getCurrentTimeStep()
        self._timestep_widget.setText("current timestep: " + str(step))
        super(self.__class__, self).update()        

class OccursWidget(QTextEdit):
    def __init__(self): 
        super(self.__class__, self).__init__()
        self._model = None
        self.setReadOnly(True)
        self.resize(400, 400)
        self.setWindowTitle("Occurs")
        self.setFontPointSize(14)

    def update(self):
        action_lists = []
        max_len = 0
        self.clear()
        text = ""
        for robot in self._model.getRobots():
            action_list = robot.get_actions_fun()
            action_lists.append(action_list)
            max_len = max(max_len, len(action_list))
        for i in xrange(0, max_len):
            for action_list in action_lists:
                if len(action_list) > i:
                    if action_list[i] is not None:
                        if i == self._model.getCurrentTimeStep():
                            text = text + "<font color = green>" + action_list[i]  + "<br>\n" + "</font>"               
                        else:
                            text = text + action_list[i] + "<br>\n"
        self.setHtml(text)
        super(self.__class__, self).update()

    def set_model(self, model):
        self._model = model
        if self._model is not None:
            self._model.add_window(self)

class ControlSplitter(QSplitter):
    def __init__(self):
        super(self.__class__, self).__init__(Qt.Vertical)
        self._control_widget = ControlWidget()
        self._occurs_widget = OccursWidget()        
        self.addWidget(self._control_widget)
        self.addWidget(self._occurs_widget)
        self.setSizes([90, self.size().height() - 90]) 

    def set_visualizer(self, visualizer):
        self._control_widget.set_visualizer(visualizer)

    def set_model(self, model):
        self._occurs_widget.set_model(model)

        
