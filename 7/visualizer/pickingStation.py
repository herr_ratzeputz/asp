import visualizer
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class PickingStation(QGraphicsRectItem):
	def __init__(self):
		super(self.__class__, self).__init__()
		self._id = None
		self._x  = 0
		self._y  = 0
		self._dragged = False
		self._enable_drag = False

		self._items = []
		self._items.append(QGraphicsRectItem())
		self._items.append(QGraphicsRectItem())
		self._items[0].setParentItem(self)
		self._items[1].setParentItem(self)
		self.set_color(QColor(255, 255, 0))

		self.setAcceptedMouseButtons(Qt.MouseButtons(1))

	def set_id(self, picking_station_id):
		self._id = picking_station_id

	def set_starting_position(self, x, y):
		self.set_position( x, y)

	def set_position(self, x, y):
		 self._x = x
		 self._y = y

	def set_color(self, color):
		color.setAlpha(150)
		brush = QBrush(color)	
		brush2 = QBrush(QColor(0,0,0,150))
	
		self.setBrush(brush)	
		self._items[0].setBrush(brush2)
		self._items[1].setBrush(brush2)

	def setRect(self, x, y, w, h):
		super(self.__class__, self).setRect(x, y, w, h)
		self._items[0].setRect(x + w/5, y, w/5, h)
		self._items[1].setRect(x + w/5 * 3, y, w/5, h)

	def enable_drag(self, enable):
		self._enable_drag = enable

	def mousePressEvent(self, event):
		if self._enable_drag:
			self._dragged = True

	def mouseReleaseEvent(self, event):
		if self._dragged == True:
			self._dragged = False
			for view in self.scene().views():
				if isinstance(view, visualizer.Visualizer):
					view.moveItem(self, self.rect().topLeft())

	def mouseMoveEvent(self, event):
		if self._dragged == True:
			rect = self.rect()
			rect.moveTo(event.pos().x(), event.pos().y())
			self.setRect(rect.x(), rect.y(), rect.width(), rect.height())

	def to_fun_str(self):
		return ("init(object(pickingStation," 
                + str(self._id) + "),value(at,(" + str(self._x) 
                + "," + str(self._y) + "))).")

	def get_id(self):
		return self._id

	def get_positionx(self):
		return self._x

	def get_positiony(self):
		return self._y	

