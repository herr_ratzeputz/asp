import visualizer
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class Shelf(QGraphicsEllipseItem):
    def __init__(self):
        super(self.__class__, self).__init__()
        self._ID = None
        self._firstX = None
        self._firstY = None
        self._x  = 0
        self._y  = 0
        self._carried = None
        self._items   = []
        self._dragged = False
        self._enable_drag = False
        self.setAcceptedMouseButtons(Qt.MouseButtons(1))
        self.updateTooltip()

    def mousePressEvent(self, event):
        if self._enable_drag:
            self._dragged = True

    def mouseReleaseEvent(self, event):
        if self._dragged == True:
            self._dragged = False
            for view in self.scene().views():
                if isinstance(view, visualizer.Visualizer):
                    view.moveItem(self, self.rect().topLeft())

    def mouseMoveEvent(self, event):
        if self._dragged == True:
            rect = self.rect()
            rect.moveTo(event.pos().x(), event.pos().y())
            self.setRect(rect)

    def enable_drag(self, enable):
        self._enable_drag = enable

    def to_fun_str(self):
        s = ("init(object(shelf," 
            + str(self._ID) + "),value(at,(" 
            + str(self._firstX) + "," 
            + str(self._firstY) + "))).")
        for item in self._items:
            s += ("init(object(product," 
                    + str(item[0]) + "),value(on,(" 
                    + str(self._ID) + "," 
                    + str(item[1]) + "))).")
        return s

    def set_id(self, shelfID):
        self._ID = shelfID
        self.updateTooltip()

    def set_starting_position(self, x, y):
        if self._firstX == None: self._x = x
        if self._firstY == None: self._y = y
        self._firstX = x
        self._firstY = y

    def set_position(self, x, y):
        self._x = x
        self._y = y        

    def setCarried(self, robot):
        if robot == self._carried: return
        old = self._carried
        self._carried = robot
        if old           != None: old.setCarries(None)
        if self._carried != None: self._carried.setCarries(self)

    def restart(self):
        self._x = self._firstX
        self._y = self._firstY 

    def setItem(self, itemID, amount = 0):
        item = self._findItem(itemID)
        if item == None: self._items.append((itemID, amount, 0))
        else:
            self._items.remove(item)
            self._items.append((itemID, amount, item[2]))
        self.updateTooltip()

    def addItem(self, itemID, amount = 0):
        item = self._findItem(itemID)
        if item == None: self._items.append((itemID, amount, 0))
        else: 
            if amount == 0: self._items.append((itemID, 0, 0))
            else:           self._items.append((itemID, item[1] + amount, 0))
            self._items.remove(item)
        self.updateTooltip()

    def removeItem(self, itemID, amount):
        item = self._findItem(itemID)
        if item != None:
            self._items.append((itemID, item[1], item[2] + amount)) 
            self._items.remove(item)
        self.updateTooltip()
    
    def put_back_item(self, itemID, amount):
        item = self._findItem(itemID)    
        if item != None:
            self._items.append((itemID, item[1], item[2] - amount)) 
            self._items.remove(item)
        self.updateTooltip()

    def delete_item(self, itemID):
        item = self._findItem(itemID)
        if item != None:  
            self._items.remove(item)
        self.updateTooltip()      

    def contains(self, itemID, amount = 0):
        item = self._findItem(self, itemID, amount = 0)
        if item == None: return 0
        amount2 = item[1] <= item[2]
        if amount < amount2: return 0
        return 1

    def updateTooltip(self):
        tooltip = "shelf(" + str(self._ID) + ")\nitems:\n"
        for item in self._items:
            tooltip = tooltip + str(item) + "\n"
        self.setToolTip(tooltip)

    def set_color(self, color):
        brush = QBrush(color)    
        self.setBrush(brush)            

    def getItemAmount(self, itemID):
        item = self._findItem(itemID)
        if item == None: return 0
        return item[1] - item[2]
   
    def _findItem(self, itemID): 
        for item in self._items:
            if str(itemID) == str(item[0]): return item
        return None

    def get_id(self):
        return self._ID

    def get_positionx(self):
        return self._x

    def get_positiony(self):
        return self._y

    def getCarried(self):
        return self._carried

    def getItems(self):
        return self._items
