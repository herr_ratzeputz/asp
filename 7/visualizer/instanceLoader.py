from request import *
from model import *
import os.path
import clingo
import argparse
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class InstanceFileDialog(QTreeView):
    def __init__(self, directory = None):
        super(self.__class__, self).__init__()
        self._model = QFileSystemModel()
        self._model.setRootPath(QDir.rootPath())
        self.setModel(self._model)
        if directory is not None:
            self.setRootIndex(self._model.index(directory))
        else:
            self.setRootIndex(self._model.index(QDir.rootPath()))
        self.setColumnWidth(0,self.width())
        self._instance_loader = None

        self._model.setNameFilters(["*.lp"])

    def resizeEvent (self, event):
        self.setColumnWidth(0,self.width())
        return super(self.__class__, self).resizeEvent(event)

    def mouseDoubleClickEvent (self, event):
        indexes = self.selectedIndexes()
        if len(indexes) != 0:
            if not self._model.isDir(indexes[0]) and not self._instance_loader is None:
                self._instance_loader.loadInstance(self._model.filePath(indexes[0]))
        return super(self.__class__, self).mouseDoubleClickEvent(event)

    def keyPressEvent (self, event):
        if event.key() == Qt.Key_Return:
            indexes = self.selectedIndexes()
            if len(indexes) != 0:
                if not self._model.isDir(indexes[0]) and not self._instance_loader is None:
                    self._instance_loader.loadInstance(self._model.filePath(indexes[0]))
        return super(self.__class__, self).keyPressEvent(event)

    def setInstanceLoader(self, loader):
        self._instance_loader = loader

class InstanceLoader(object):
    def __init__(self):
        self._incremental = False
        self._online        = False
        self._model = Model()
        self._control = None
        self._visualizer = None

    def onModel(self, m):
        #inits
        requests = []
        orders   = []
        for x in m.symbols(atoms=True):
            if x.name == "init" and len(x.arguments) == 2:
                try:
                    obj  = x.arguments[0]
                    atom = x.arguments[1]

                    if atom.name == "value":
                        if str(atom.arguments[0]) == "at":
                            position = atom.arguments[1]
                            if obj.name == "object":
                                if str(obj.arguments[0]) == "robot":                                #init robots
                                    robot = self._model.getRobot2(str(obj.arguments[1]))
                                    robot.set_starting_position(position.arguments[0].number, position.arguments[1].number)

                                elif str(obj.arguments[0]) == "shelf":                                #init shelfs
                                    shelf = self._model.getShelf2(str(obj.arguments[1]))
                                    shelf.set_starting_position(position.arguments[0].number, position.arguments[1].number)

                                elif str(obj.arguments[0]) == "pickingStation":                    #init picking station
                                    station = self._model.get_station2(str(obj.arguments[1]))
                                    station.set_position(position.arguments[0].number, position.arguments[1].number)

                                elif str(obj.arguments[0]) == "node":                                #init nodes
                                    self._model.add_node(position.arguments[0].number, position.arguments[1].number)
 
                                elif str(obj.arguments[0]) == "highway":                                #init highways
                                    self._model.add_highway(position.arguments[0].number, position.arguments[1].number)
           
                                else:
                                    self._model.addInit(x)
                            else:
                                self._model.addInit(x)

                        elif str(atom.arguments[0]) == "on":
                            item = atom.arguments[1]
                            if obj.name == "object":
                                if str(obj.arguments[0]) == "product":                                #init products
                                    shelf = self._model.getShelf2(item.arguments[0])
                                    shelf.setItem(obj.arguments[1].number, item.arguments[1])

                        elif str(atom.arguments[0]) == "line":                                     #orders (line)
                            value = atom.arguments[1]
                            if str(obj.arguments[0]) == "order":
                                request = Request()
                                request.set_id(str(obj.arguments[1]))
                                request.set_item_id(str(atom.arguments[1].arguments[0]))
                                request.set_requested_amount(atom.arguments[1].arguments[1].number)
                                requests.append(request)
                                for order in orders:
                                    if order[0] == request.get_id():
                                        request.set_station_id(str(order[1]))

                        elif str(atom.arguments[0]) == "pickingStation":                     #orders (picking stations)
                            value = atom.arguments[1]
                            if str(obj.arguments[0]) == "order":
                                orders.append((str(obj.arguments[1]), str(atom.arguments[1])))
                                for request in requests:
                                    if str(obj.arguments[1]) == request.get_id():
                                        request.set_station_id(str(atom.arguments[1]))
                        else:
                            self._model.addInit(x)
                    else:
                        self._model.addInit(x)

                except:
                    print ("invalid init format, expecting: ",
                             "init(object(robot, RobotID), value(at, (X, Y))) or",
                             "init(object(shelf, ShelfID), value(at, (X, Y))), or ",
                             "init(object(pickingStation, PickingStationID), value(at, (X, Y))) or",
                             "init(object(order, OrderID), value(line, (RequestID, RequestedAmount))) or",
                             "init(object(order, OrderID), value(pickingStation, PickingStationID)) or",
                             "init(object(product, ProductID), value(on, (ShelfID, ItemAmount))")

        for request in requests:
            self._model.addRequest(request)

    def loadInstance(self, instance):
        self._visualizer.stop_timer()
        self._model.clear()
        if not os.path.isfile(instance):
            print "can't open file: ", instance
            return -1

        self._control = clingo.Control()
        self._control.load(instance)
        self._model._file_path = instance
        
        print "ground..."
        self._control.ground([("base", [])])
        print "solve..."
        self._control.solve(self.onModel, [])
        if self._visualizer is not None:
            self._visualizer.update()
            rect = self._visualizer.sceneRect()
            position  = self._visualizer.mapFromScene(QPoint(rect.x(), rect.y()))
            position2 = self._visualizer.mapFromScene(QPoint(rect.x() + rect.width(), rect.y() + rect.height()))
            pixmap = self._visualizer.grab(QRect(position.x(), position.y(), position2.x() - position.x(), position2.y() - position.y()))
            pixmap.save(instance[0 : instance.rfind('.')] + ".png")
        self._model.enable_drag(True)
        self._model.update_windows()
        return 0

    def set_visualizer(self, visualizer):
        self._visualizer = visualizer

    def getAtoms(self):
        return self._atoms

    def getModel(self):
        return self._model
