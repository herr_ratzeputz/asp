import sys
import time
import os
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from instanceLoader import *
from visualizer import *
from kiwaSocket import *
from itemWindow import *
from controlWidget import *

VERSION = "0.7.7"

class InputDialog(QWidget):
    def __init__(self):
        super(self.__class__, self).__init__()
        
        self.setWindowTitle("Initialize solver")
        self._textbox = QLineEdit(self)
        self._textbox.setText("python2 solver.py --port 5000")
        self._ok_button = QPushButton('Ok', self)
        self._cancel_button = QPushButton('Cancel', self)
        self._function = None

        self._ok_button.clicked.connect(self.on_ok)
        self._cancel_button.clicked.connect(self.on_cancel)
        self.setFixedSize(320, 80)
        self._textbox.resize(280,30)
        self._ok_button.move(20,30)
        self._cancel_button.move(140,30)

    def set_fuction(self, function):
        self._function = function

    def on_ok(self, event):
        self._function(self._textbox.text())
        self.hide()
    def on_cancel(self, event):
        self.hide()

class ServerDialog(QWidget):
    def __init__(self):
        super(self.__class__, self).__init__()
        
        self.setWindowTitle("Solving")
        self._host_text = QLineEdit(self)
        self._host_text.setText("host: ")
        self._host_text.setReadOnly(True)
        self._port_text = QLineEdit(self)
        self._port_text.setText("port: ")
        self._port_text.setReadOnly(True)
        self._host_textbox = QLineEdit(self)
        self._host_textbox.setText("127.0.0.1")
        self._port_textbox = QLineEdit(self)
        self._port_textbox.setText("5000")
        self._ok_button = QPushButton('Ok', self)
        self._cancel_button = QPushButton('Cancel', self)
        self._function = None

        self._ok_button.clicked.connect(self.on_ok)
        self._cancel_button.clicked.connect(self.on_cancel)
        self.setFixedSize(320, 110)
        self._host_text.resize(140, 30)
        self._port_text.resize(140, 30)
        self._host_textbox.resize(140,30)
        self._port_textbox.resize(140,30)
        self._host_text.move(0,0)
        self._port_text.move(0,40)
        self._host_textbox.move(140,0)
        self._port_textbox.move(140,40)
        self._ok_button.move(20,80)
        self._cancel_button.move(140,80)

    def set_fuction(self, function):
        self._function = function

    def on_ok(self, event):
        try:
            self._function(self._host_textbox.text(), int(self._port_textbox.text()))
        except(ValueError):
            print "the port must be an integer value"
        self.hide()
    def on_cancel(self, event):
        self.hide()

class GridSizeDialog(QWidget):
    def __init__(self):
        super(self.__class__, self).__init__()
        
        self._model = None
        self._visualizer = None
        self._item_window = None
        self.setWindowTitle("Grid size")
        self._width_text = QLineEdit(self)
        self._width_text.setText("x: ")
        self._width_text.setReadOnly(True)
        self._height_text = QLineEdit(self)
        self._height_text.setText("y: ")
        self._height_text.setReadOnly(True)
        self._width_textbox = QLineEdit(self)
        self._width_textbox.setText("0")
        self._height_textbox = QLineEdit(self)
        self._height_textbox.setText("0")
        self._ok_button = QPushButton('Ok', self)
        self._cancel_button = QPushButton('Cancel', self)
        self._function = None

        self._ok_button.clicked.connect(self.on_ok)
        self._cancel_button.clicked.connect(self.on_cancel)
        self.setFixedSize(320, 110)
        self._width_text.resize(140, 30)
        self._height_text.resize(140, 30)
        self._width_textbox.resize(140,30)
        self._height_textbox.resize(140,30)
        self._width_text.move(0,0)
        self._height_text.move(0,40)
        self._width_textbox.move(140,0)
        self._height_textbox.move(140,40)
        self._ok_button.move(20,80)
        self._cancel_button.move(140,80)
    
    def set_model(self, model):
        self._model = model

    def set_visualizer(self, visualizer):
        self._visualizer = visualizer

    def on_ok(self, event):
        self.hide()
        if not self._model.is_drag_enabled(): return
        try:
            self._model.setGridSize(int(self._width_textbox.text()), int(self._height_textbox.text()))
        except ValueError:
            print "the product id and the product counts must be integer values"
        self._visualizer.update()

    def on_cancel(self, event):
        self.hide()

class OrderDialog(QWidget):
    def __init__(self):
        super(self.__class__, self).__init__()
        
        self._model = None
        self.setWindowTitle("Add order")
        self.setFixedSize(320, 190)

        self._id_text = QLineEdit(self)
        self._id_text.setText("order id: ")
        self._id_text.setReadOnly(True)
        self._id_text.move(0,0)
        self._id_text.resize(140, 30)
        self._id_textbox = QLineEdit(self)
        self._id_textbox.setText("0")
        self._id_textbox.move(140,0)
        self._id_textbox.resize(140,30)

        self._product_id_text = QLineEdit(self)
        self._product_id_text.setText("product id: ")
        self._product_id_text.setReadOnly(True)
        self._product_id_text.move(0,40)
        self._product_id_text.resize(140, 30)
        self._product_id_textbox = QLineEdit(self)
        self._product_id_textbox.setText("0")
        self._product_id_textbox.move(140,40)
        self._product_id_textbox.resize(140,30)

        self._product_amount_text = QLineEdit(self)
        self._product_amount_text.setText("product amount: ")
        self._product_amount_text.setReadOnly(True)
        self._product_amount_text.move(0,80)
        self._product_amount_text.resize(140, 30)
        self._product_amount_textbox = QLineEdit(self)
        self._product_amount_textbox.setText("0")
        self._product_amount_textbox.move(140,80)
        self._product_amount_textbox.resize(140,30)

        self._ps_text = QLineEdit(self)
        self._ps_text.setText("picking station id: ")
        self._ps_text.setReadOnly(True)
        self._ps_text.move(0,120)
        self._ps_text.resize(140, 30)
        self._ps_textbox = QLineEdit(self)
        self._ps_textbox.setText("0")
        self._ps_textbox.move(140,120)
        self._ps_textbox.resize(140,30)

        self._ok_button = QPushButton('Ok', self)
        self._ok_button.move(20,160)
        self._ok_button.clicked.connect(self.on_ok)
        self._cancel_button = QPushButton('Cancel', self)
        self._cancel_button.move(140,160)
        self._cancel_button.clicked.connect(self.on_cancel)

    def on_ok(self, event):
        if self._model is None:
            return 
        request = Request()
        try:
            request.set_id(self._id_textbox.text())
            request.set_item_id(self._product_id_textbox.text())
            request.set_requested_amount(int(self._product_amount_textbox.text()))
            request.set_station_id(self._ps_textbox.text())
            self._model.addRequest(request)
            self._model.update_windows()
        except:
            print "failed to add new request"
            return
        self.hide()
    
    def on_cancel(self, event):
        self.hide()

    def set_model(self, model):
        self._model = model

class OrderTable(QTableWidget):
    def __init__(self, parent): 
        super(self.__class__, self).__init__(parent)
        self._model = None
        self.setColumnCount(6)
        self.setHorizontalHeaderLabels(["Order ID", "Picking Station", "Product", "Product Amount", "Delivered", "Open"])

        self.setContextMenuPolicy(Qt.DefaultContextMenu)
        self._menu = QMenu()
        self._menu.setParent(self)

        self._order_dialog = OrderDialog()

    def contextMenuEvent(self, event):

        if not self._model.is_drag_enabled(): return 
        order = None
        item = self.itemAt(event.x(), event.y())
        if item is not None:
            row = self.row(item)
            count = 0
            for order2 in self._model.getRequests():
                if count == row:
                    order = order2
                    break
                count = count + 1

        self._menu.clear()
        add_order_action = QAction("add order", self)
        add_order_action.setShortcut("Ctrl + R")
        add_order_action.setStatusTip("Adds a new order")
        add_order_action.triggered.connect(self.add_order)
        self._menu.addAction(add_order_action)

        if order is not None:
            remove_order_action = QAction("remove order", self)
            remove_order_action.setShortcut("Ctrl + R")
            remove_order_action.setStatusTip("Removes the selected order")
            remove_order_action.triggered.connect(lambda: self.remove_order(order))
            self._menu.addAction(remove_order_action)

        self._menu.popup(QPoint(event.x(),event.y()))

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton or event.button() == Qt.MiddleButton:
            self._menu.hide()
        super(self.__class__, self).mousePressEvent(event)

    def set_model(self, model):
        self._model = model
        self._order_dialog.set_model(model)

    def add_order(self):
        self._menu.hide()
        self._order_dialog.show()

    def remove_order(self, order):
        self._menu.hide()
        self._model.remove_order(order)
        self._model.update_windows()

class OrderWidget(QSplitter):
    def __init__(self): 
        super(self.__class__, self).__init__(Qt.Vertical)
        self._model = None
        self._table = OrderTable(self)
        self.resize(600, 400)
        self.setWindowTitle("Orders")

        self._deliver_widget = QTextEdit()
        self._deliver_widget.setReadOnly(True)
        self.addWidget(self._deliver_widget)

    def update(self):
        self._deliver_widget.setText(self._model.get_delivered(self._model.getCurrentTimeStep()-1))
        red_brush   = QBrush(QColor(200, 100, 100))
        white_brush = QBrush(QColor(255, 255, 255))
        changed_requests = self._table.rowCount() != len(self._model.getRequests())
        self._table.setRowCount(len(self._model.getRequests()))
        count = 0
        if not changed_requests:
            for order in self._model.getRequests():
                table_item_id = self._table.item(count, 0)
                table_item_station= self._table.item(count, 1)
                table_item_product= self._table.item(count, 2)
                table_item_amount= self._table.item(count, 3)
                if table_item_id is None or table_item_station is None or table_item_product is None or table_item_amount is None:
                    changed_requests = True
                    break
                elif not(str(order.get_id()) == table_item_id.text() and
                    str(order.get_station_id()) == table_item_station.text() and
                    str(order.get_item_id()) == table_item_product.text() and          
                    str(order.get_requested_amount()) == table_item_amount.text()):
                    print "hallo2"
                    changed_requests = True
                    break       
                count = count + 1     
 
        count = 0
        for order in self._model.getRequests():
            table_item_delivered = self._table.item(count, 4)
            if table_item_delivered is not None:
                if str(order.get_received_amount()) == str(table_item_delivered.text()) or changed_requests:
                    for ii in xrange(0, self._table.columnCount()):
                        self._table.item(count, ii).setBackground(white_brush)
                else:
                    for ii in xrange(0, self._table.columnCount()):
                        self._table.item(count, ii).setBackground(red_brush)
            self.set_item_text(count, 0, str(order.get_id()))
            self.set_item_text(count, 1, str(order.get_station_id()))
            self.set_item_text(count, 2, str(order.get_item_id()))
            self.set_item_text(count, 3, str(order.get_requested_amount()))
            self.set_item_text(count, 4, str(order.get_received_amount()))
            self.set_item_text(count, 5, str(order.get_requested_amount() - order.get_received_amount()))
            count = count + 1
        super(self.__class__, self).update()

    def set_item_text(self, column, row, text):
        item = self._table.item(column, row)
        if item is None:
            self._table.setItem(column, row, QTableWidgetItem(text))
        else:
            item.setText(text)

    def set_model(self, model):
        self._model = model
        if self._model is not None:
            self._model.add_window(self)
        self._table.set_model(model)

class KiwaWindow(QMainWindow):
    def __init__(self):                        #init main window

        self._control = None
        self._parser = argparse.ArgumentParser()
        self._parser.add_argument("-d", "--directory", help="directory to load all instances from",
                                            type=str)
        self._parser.add_argument("-v", "--version",
                                            help="show the current version", action="version",
                                            version=VERSION)
        self._args = self._parser.parse_args()

        super(self.__class__, self).__init__()

        self._kSocket  = KiwaSocket()

        self._splitter = QSplitter() 
        self._menuSolver = None
        self._itemWindow = ItemWindow()

        self._initialize_solver_dialog = InputDialog()
        self._run_solver_dialog = ServerDialog()
        self._initialize_solver_dialog.set_fuction(self._kSocket.runSolver)
        self._run_solver_dialog.set_fuction(self.startSolving)

        self._grid_size_dialog = GridSizeDialog()

        self._order_widget = OrderWidget()

        self.initGUI()                            #init GUI

        self._instanceFileDialog = InstanceFileDialog(self._args.directory)
        self._splitter.addWidget(self._instanceFileDialog)

        self._visualizer = Visualizer()    #init visualizer window
        self.setCentralWidget(self._splitter)
        self._visualizer.show()
        self._splitter.addWidget(self._visualizer)


        self.setWindowTitle("Visualizer")        #init the window
        self.resize(1000, 600)
        self.move(0,0)
        self.show()

        QToolTip.setFont(QFont('SansSerif', 10))

        self._iLoader = InstanceLoader()
        self._instanceFileDialog.setInstanceLoader(self._iLoader)
        self._visualizer.setModel(self._iLoader.getModel())

        self._grid_size_dialog.set_model(self._iLoader.getModel())
        self._grid_size_dialog.set_visualizer(self._visualizer)    
    
        self._order_widget.set_model(self._iLoader.getModel())

        self._splitter.setSizes([200, 600, 200])
        self._iLoader.set_visualizer(self._visualizer)
        self._itemWindow.setModel(self._iLoader.getModel())
        self._controlSplitter = ControlSplitter()
        self._splitter.addWidget(self._controlSplitter)
        self._controlSplitter.set_visualizer(self._visualizer)
        self._controlSplitter.set_model(self._iLoader.getModel())

    def initGUI(self):                        #init GUI fuction
                                                    #create the menu
        exitAction = QAction("Exit", self)
        exitAction.setShortcut("Ctrl+Q")
        exitAction.setStatusTip("Leave the Application")
        exitAction.triggered.connect(self.closeApplication)

        newInstanceAction = QAction("New instance", self)
        newInstanceAction.setShortcut("Ctrl+N")
        newInstanceAction.setStatusTip("Creates a clear instance")
        newInstanceAction.triggered.connect(self.newInstance)

        loadInstanceAction = QAction("Load instance", self)
        loadInstanceAction.setShortcut("Ctrl+L")
        loadInstanceAction.setStatusTip("Loads a new instance file")
        loadInstanceAction.triggered.connect(self.loadInstance)

        visualizeAnswerAction = QAction("Visualize answer", self)
        visualizeAnswerAction.setShortcut("Ctrl+A")
        visualizeAnswerAction.setStatusTip("Loads an answer file")
        visualizeAnswerAction.triggered.connect(self.load_answer)

        self._saveInstanceAction = QAction("Save instance", self)
        self._saveInstanceAction.setShortcut("Ctrl+S")
        self._saveInstanceAction.setStatusTip("Saves the current instance to a file")
        self._saveInstanceAction.triggered.connect(self.saveInstance)

        self._saveAnswerAction = QAction("Save answer", self)
        self._saveAnswerAction.setShortcut("Ctrl+D")
        self._saveAnswerAction.setStatusTip("Saves the current answer set to a file")
        self._saveAnswerAction.triggered.connect(self.save_answer)

        crate_all_pic_action = QAction("Create all png files", self)
        crate_all_pic_action.setShortcut("Ctrl+P")
        crate_all_pic_action.setStatusTip("Creates png files from all instance files in the given directory and all sub-directories")
        crate_all_pic_action.triggered.connect(self.create_all_pictures)

        runSolverAction = QAction("Initialize solver", self)
        runSolverAction.setShortcut("Ctrl+R")
        runSolverAction.setStatusTip("Runs a solver script")
        runSolverAction.triggered.connect(self._initialize_solver_dialog.show)

        startSolvingAction = QAction("Solve", self)
        startSolvingAction.setShortcut("Ctrl+E")
        startSolvingAction.setStatusTip("Starts solving")
        startSolvingAction.triggered.connect(self._run_solver_dialog.show)

        doAll = QAction("Run+Solve", self)
        doAll.setShortcut("Ctrl+W")
        doAll.setStatusTip("Run and Solve with defaults")
        doAll.triggered.connect(self.run_solve)
        
        showItemsWindowAction = QAction("Show products", self)
        showItemsWindowAction.setShortcut("Ctrl+I")
        showItemsWindowAction.setStatusTip("Shows a new window that contains all products")
        showItemsWindowAction.triggered.connect(self._itemWindow.show)

        showOrderWindowAction = QAction("Show orders", self)
        showOrderWindowAction.setShortcut("Ctrl+U")
        showOrderWindowAction.setStatusTip("Shows a new window that lists all orders")
        showOrderWindowAction.triggered.connect(self._order_widget.show)

        setGridSizeAction = QAction("Set grid size", self)
        setGridSizeAction.setShortcut("Ctrl+G")
        setGridSizeAction.setStatusTip("Shows a window to change the grid size")
        setGridSizeAction.triggered.connect(self._grid_size_dialog.show)

        self.statusBar()

        menuBar = self.menuBar()
        menuFile = menuBar.addMenu("File")
        menuFile.addAction(loadInstanceAction)
        menuFile.addAction(newInstanceAction)
        menuFile.addAction(self._saveInstanceAction)
        menuFile.addAction(visualizeAnswerAction)
        menuFile.addAction(self._saveAnswerAction)
        menuFile.addAction(crate_all_pic_action)
        menuFile.addAction(exitAction)

        self._menuSolver = menuBar.addMenu("Solver")
        self._menuSolver.addAction(startSolvingAction)
        self._menuSolver.addAction(runSolverAction)
        self._menuSolver.addAction(doAll)
        
        self._menuWindow = menuBar.addMenu("Windows")
        self._menuWindow.addAction(showItemsWindowAction)
        self._menuWindow.addAction(showOrderWindowAction)
        self._menuWindow.addAction(setGridSizeAction)

    def resizeEvent(self, event):
        super(self.__class__, self).resizeEvent(event)
        size = self._splitter.sizes()[1]
        self._visualizer.resize(size, self._visualizer.height())

    def startSolving(self, host, port):
        if self._kSocket.connect(host, port) == -1: return -1
        self.setWindowTitle("Visualizer(Solving...please wait)")
        self._kSocket.setModel(self._iLoader.getModel())
        self._kSocket.solve()
        self.setWindowTitle("Visualizer") 
        return 0

    def closeEvent(self, event):
        self._itemWindow.close()
        self._order_widget.close()
        return super(self.__class__, self).closeEvent(event)

    def closeApplication(self):            #close app function
        sys.exit()

    def exit(self):
        self._kSocket.close()

    def loadInstance(self):
        fileName = QFileDialog().getOpenFileName(self, "Open file", ".", 
                                                                    "Lp files (*.lp);; All files (*))")
        if fileName is None or len(fileName[0]) == 0: return
        if self._iLoader.loadInstance(fileName[0]) == -1:
            return -1        
        self._visualizer.setModel(self._iLoader.getModel())

    def newInstance(self):
        self._iLoader.getModel().clear()
        self._iLoader.getModel().update_windows()
        self._visualizer.update()

    def load_answer(self):
        fileName = QFileDialog().getOpenFileName(self, "Open file", ".", 
                                                                    "Lp files (*.lp);; All files (*))")
        if fileName is None or len(fileName[0]) == 0: return
        self._iLoader.getModel().load_answer(fileName[0])

    def create_all_pictures(self):
        directory = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        self.create_pictures_in_directory(directory)
      
    def create_pictures_in_directory(self, directory):
        for filename in os.listdir(directory):
            full_filename = directory + "/" + filename
            if filename.endswith(".lp"): 
                self._iLoader.loadInstance(full_filename)
            elif os.path.isdir(full_filename):
                self.create_pictures_in_directory(full_filename)
  
    def saveInstance(self):
        fileName = QFileDialog().getSaveFileName(self, "Open file", ".", 
                                                                    "Lp files (*.lp);; All files (*))")
        if fileName is None or len(fileName[0]) == 0: return
        self._visualizer.getModel().saveToFile(fileName[0])

    def save_answer(self):
        fileName = QFileDialog().getSaveFileName(self, "Open file", ".", 
                                                                    "Lp files (*.lp);; All files (*))")
        if fileName is None or len(fileName[0]) == 0: return  
        self._visualizer.getModel().save_answer(fileName[0])

    def run_solve(self):
        self._kSocket.runSolver("python2 solver.py --port 5000")
        time.sleep(2)
        self.startSolving("127.0.0.1", 5000)

        
app = QApplication(sys.argv)
wnd = KiwaWindow()
value = app.exec_()
wnd.exit()
sys.exit(value)
