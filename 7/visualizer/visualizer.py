from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from ConfigParser import ConfigParser
from model import *

import robot
import shelf
import pickingStation

def read_hex_from_config(config_parser, section, value, default=0):
    try: 
        return int(config_parser.get(section, value),16)
    except:
        return default
    
class MainScene(QGraphicsScene):
    def __init__(self):
        super(self.__class__, self).__init__()

class Visualizer(QGraphicsView):
    def __init__(self):
        super(self.__class__, self).__init__()
        self._scene = MainScene()
        self.setScene(self._scene)

        config_parser = ConfigParser()
        config_parser.read("init.cfg")
 
        self._model = None

        self._objectSize = 0
        self._screen_width  = 800
        self._screen_height = 600
        self._border_size = 0             #the size of the borders of the grid

        self._h_distance = 0              #the size of one grid cell
        self._w_distance = 0

        self._colorMinRobot = QColor(read_hex_from_config(config_parser, "Visualizer", "Color_robot_min", 0xff0000))
        self._colorMaxRobot = QColor(read_hex_from_config(config_parser, "Visualizer", "Color_robot_max", 0xffff00))
        self._colorMinShelf = QColor(read_hex_from_config(config_parser, "Visualizer", "Color_shelf_min", 0x009b00))
        self._colorMaxShelf = QColor(read_hex_from_config(config_parser, "Visualizer", "Color_shelf_max", 0x009bff))
        self._brush_disabled_node = QBrush(QColor(read_hex_from_config(config_parser, "Visualizer", "Color_disabled_node", 0x323232)))
        self._brush_highway = QBrush(QColor(read_hex_from_config(config_parser, "Visualizer", "Color_highway", 0xff0000)))

        self._itemsInScene = []
        try:
            self._stepTime = config_parser.getint("Visualizer", "Step_time")
        except:
            self._stepTime = 1200

        self._timer = None
        self._timerCount = 1

        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setContextMenuPolicy(Qt.DefaultContextMenu)
        self._menu = QMenu()
        self._menu.setParent(self)

    def contextMenuEvent(self, event):
        if self._model == None:
            return

        point = self.mapToScene(event.x(), event.y())
        x = int(point.x() / self._w_distance + 1)
        y = int(point.y() / self._h_distance + 1)

        if x <= 0 or y <= 0 or x > self._model.getGridSizeX() or y > self._model.getGridSizeY():
            return
        if not self._model.is_drag_enabled(): return 

        self._menu.clear()

        if self._model.is_node(x,y):
            removeNodeAction = QAction("disable node", self)
            removeNodeAction.setShortcut("Ctrl + R")
            removeNodeAction.setStatusTip("Disables the selected node")
            removeNodeAction.triggered.connect(lambda: self._remove_node(x, y))
            self._menu.addAction(removeNodeAction)   
        else:
            addNodeAction = QAction("enable node", self)
            addNodeAction.setShortcut("Ctrl + R")
            addNodeAction.setStatusTip("Enables the selected node")
            addNodeAction.triggered.connect(lambda: self._add_node(x, y))
            self._menu.addAction(addNodeAction) 

        if self._model.is_highway(x,y):
            removeHighwayAction = QAction("remove highway", self)
            removeHighwayAction.setShortcut("Ctrl + R")
            removeHighwayAction.setStatusTip("Removes a highway from the selected node")
            removeHighwayAction.triggered.connect(lambda: self._remove_highway(x,y))
            self._menu.addAction(removeHighwayAction)                 
        elif self._model.is_node(x,y):
            addHighwayAction = QAction("add highway", self)
            addHighwayAction.setShortcut("Ctrl + R")
            addHighwayAction.setStatusTip("Adds a highway to the selected node")
            addHighwayAction.triggered.connect(lambda: self._add_highway(x,y))
            self._menu.addAction(addHighwayAction)                   

        robot = self._model.getRobotInNode(x,y)
        if robot != None:
            removeRobotAction = QAction("remove robot", self)
            removeRobotAction.setShortcut("Ctrl + R")
            removeRobotAction.setStatusTip("Removes a robot from the selected node")
            removeRobotAction.triggered.connect(lambda: self._removeRobot(robot))
            self._menu.addAction(removeRobotAction)
        elif self._model.is_node(x,y):
            addRobotAction = QAction("add robot", self)
            addRobotAction.setShortcut("Ctrl + R")
            addRobotAction.setStatusTip("Adds a robot to the selected node")
            addRobotAction.triggered.connect(lambda: self._addRobot(x, y))
            self._menu.addAction(addRobotAction)

        shelf = self._model.getShelfInNode(x,y)
        if shelf != None:
            removeShelfAction = QAction("remove shelf", self)
            removeShelfAction.setShortcut("Ctrl + R")
            removeShelfAction.setStatusTip("Removes a shelf from the selected node")
            removeShelfAction.triggered.connect(lambda: self._removeShelf(shelf))
            self._menu.addAction(removeShelfAction)
        elif self._model.is_node(x,y):
            add_shelfAction = QAction("add shelf", self)
            add_shelfAction.setShortcut("Ctrl + R")
            add_shelfAction.setStatusTip("Adds a shelf to the selected node")
            add_shelfAction.triggered.connect(lambda: self._add_shelf(x, y))
            self._menu.addAction(add_shelfAction)

        pickingStation = self._model.getPickingStationInNode(x,y)
        if pickingStation != None:
            removePickingStationAction = QAction("remove picking station", self)
            removePickingStationAction.setShortcut("Ctrl + R")
            removePickingStationAction.setStatusTip("Removes a picking station from the selected node")
            removePickingStationAction.triggered.connect(lambda: self._removePickingStation(pickingStation))
            self._menu.addAction(removePickingStationAction)
        elif self._model.is_node(x,y):
            addPickingStationAction = QAction("add picking station", self)
            addPickingStationAction.setShortcut("Ctrl + R")
            addPickingStationAction.setStatusTip("Adds a picking Station to the selected node")
            addPickingStationAction.triggered.connect(lambda: self._addPickingStation(x, y))
            self._menu.addAction(addPickingStationAction)           
               
        self._menu.popup(QPoint(event.x(),event.y()))

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton or event.button() == Qt.MiddleButton:
            self._menu.hide()
        super(self.__class__, self).mousePressEvent(event)

    def _add_highway(self, x, y):
        self._model.add_highway(x, y)
        self._menu.hide()
        self.update()
    
    def _remove_highway(self, x, y):
        self._model.remove_highway(x, y)
        self._menu.hide()
        self.update()

    def _add_node(self, x, y):
        self._model.add_node(x, y)
        self._menu.hide()
        self.update()
    
    def _remove_node(self, x, y):
        self._model.remove_node(x, y)
        self._menu.hide()
        self.update()

    def _addRobot(self, x, y):
        self._model.createRobotAt(x, y)
        self._menu.hide()
        self.update()
    
    def _removeRobot(self, robot):
        self._model.removeRobot(robot)
        self._menu.hide()
        self.update()

    def _add_shelf(self, x, y):
        self._model.createShelfAt(x, y)
        self._menu.hide()
        self.update()
    
    def _removeShelf(self, shelf):
        self._model.removeShelf(shelf)
        self._menu.hide()
        self.update()

    def _addPickingStation(self, x, y):
        self._model.createPickingStationAt(x, y)
        self._menu.hide()
        self.update()
    
    def _removePickingStation(self, station):
        self._model.removePickingStation(station)
        self._menu.hide()
        self.update()

    def setModel(self, model):
        self._model = model
        self.update()

    def visualize(self):
        self._model.enable_drag(False)
        self._timer = QTimer()
        self._timer.timeout.connect(lambda: self.updateModel(False))
        self._timer.start(self._stepTime / 10)

    def switch_timer(self):
        if self._timer is None:
            self.visualize()
        else:
            self.stop_timer()

    def stop_timer(self):
        if self._timer is not None:
            self._timer.stop()
            self._timerCount = 0
            self._timer = None

    def updateModel(self, forced):
        if self._model == None: return
        if self._timerCount >= 10 or forced:
            self._model.update()
            self._timerCount = 0
        else: 
            self._timerCount = self._timerCount + 1
        self.update()

    def undoModel(self):
        if self._model is None: return
        self._model.undo()
        self.update()

    def wheelEvent(self, event):
        self.scale(1 + event.angleDelta().y()/1440.0, 1 + event.angleDelta().y()/1440.0)
        event.accept()

    def resizeEvent (self, event):
        super(self.__class__, self).resizeEvent(event)
        self._screen_width  = event.size().width()
        self._screen_height = event.size().height()
        self.update()

    def update(self):
        if self._model == None: return 0
        max_grid_size = max(self._model.getGridSizeX(), self._model.getGridSizeY())
        if    max_grid_size > 20: self._border_size = 1        #set the size of the borders
        if    max_grid_size > 25: self._border_size = 2
        if    max_grid_size > 15: self._border_size = 3
        elif max_grid_size > 10: self._border_size = 4
        elif max_grid_size >  5: self._border_size = 5
        else: self._border_size = 6

        self._h_distance = (float)(self._screen_height - self._border_size) / self._model.getGridSizeY()
        self._w_distance = (float)(self._screen_width - self._border_size) / self._model.getGridSizeX()
        self._objectSize = min((self._screen_width)  / self._model.getGridSizeX(), 
                                      (self._screen_height) / self._model.getGridSizeY()) / 2
        font = QFont("", self._objectSize*0.2)

        pen = QPen()
        pen.setWidth(self._border_size)

        for item in self._itemsInScene:
            self._scene.removeItem(item)
        self._itemsInScene = []

        for i in range(0,self._model.getGridSizeY() + 1):
            line = self._scene.addLine(0, i * self._h_distance, self._screen_width - self._border_size, i * (self._h_distance), pen)
            self._itemsInScene.append(line)
        for i in range(0,self._model.getGridSizeX() + 1):
            line = self._scene.addLine(i * self._w_distance, 0, i * (self._w_distance), self._screen_height - self._border_size, pen)
            self._itemsInScene.append(line)

        for node in self._model.blocked_nodes():
            xPos = (node[0]-1) * self._w_distance
            yPos = (node[1]-1) * self._h_distance
            rect = self._scene.addRect(xPos, yPos, self._w_distance, self._h_distance, pen, self._brush_disabled_node)
            self._itemsInScene.append(rect)

        for node in self._model.get_highways():
            xPos = (node[0]-1) * self._w_distance
            yPos = (node[1]-1) * self._h_distance
            rect = self._scene.addRect(xPos, yPos, self._w_distance, self._h_distance, pen, self._brush_highway)
            self._itemsInScene.append(rect)

        for station in self._model.get_stations():        #draw picking station
            xPos = (station.get_positionx() - 1) * self._w_distance + self._border_size/2
            yPos = (station.get_positiony() - 1) * self._h_distance + self._border_size/2
            station.setRect(xPos, yPos, self._w_distance - self._border_size, self._h_distance - self._border_size)
            self._scene.addItem(station)
            self._itemsInScene.append(station)
            text = self._scene.addText("P(" + str(station.get_id()) + ")", font)
            text.setPos((station.get_positionx() - 1.0) * self._w_distance + self._border_size, 
                            (station.get_positiony() - 0.6) * self._h_distance + self._border_size)
            self._itemsInScene.append(text)

        count = 1.0
        for robot in self._model.getRobots():        #draw robots
            xPos = robot.get_positionx()
            yPos = robot.get_positiony()
            action = robot.getAction(self._model.getCurrentTimeStep())        #calculate pos offset
            if action.name == "move":
                actionX = action.arguments[0].number
                actionY = action.arguments[1].number
                xPos = float(xPos + float(actionX * float(self._timerCount * 0.1)))
                yPos = float(yPos + float(actionY * float(self._timerCount * 0.1)))

            xPos = (xPos - 0.5) * self._w_distance + self._border_size - self._objectSize / 2
            yPos = (yPos - 0.5) * self._h_distance + self._border_size - self._objectSize / 2

            color = QColor(self._colorMinRobot.red() + (self._colorMaxRobot.red() - self._colorMinRobot.red()) * float(count / len(self._model.getRobots())), 
                                self._colorMinRobot.green() + (self._colorMaxRobot.green() - self._colorMinRobot.green()) * float(count / len(self._model.getRobots())), 
                                self._colorMinRobot.blue() + (self._colorMaxRobot.blue() - self._colorMinRobot.blue()) * float(count / len(self._model.getRobots())))
            robot.setRect(xPos, yPos, self._objectSize, self._objectSize)
            robot.set_color(color)
            self._scene.addItem(robot)
            self._itemsInScene.append(robot)
            text = self._scene.addText("R(" + str(robot.get_id()) + ")", font)
            text.setPos((robot.get_positionx() - 1.0) * self._w_distance + self._border_size, 
                            (robot.get_positiony() - 1.0) * self._h_distance + self._border_size)
            self._itemsInScene.append(text)
            count += 1.0

        count = 1.0
        for shelf in self._model.get_shelves():        #draw shelves
            robot = shelf.getCarried()
            if robot != None:
                shelf.setRect(robot.rect())
            else:
                xPos = (shelf.get_positionx() - 0.5) * self._w_distance + self._border_size - self._objectSize / 2
                yPos = (shelf.get_positiony() - 0.5) * self._h_distance + self._border_size - self._objectSize / 2
                shelf.setRect(xPos, yPos, self._objectSize, self._objectSize)
            color = QColor(self._colorMinShelf.red() + (self._colorMaxShelf.red() - self._colorMinShelf.red()) * float(count / len(self._model.get_shelves())), 
                                self._colorMinShelf.green() + (self._colorMaxShelf.green() - self._colorMinShelf.green()) * float(count / len(self._model.get_shelves())), 
                                self._colorMinShelf.blue() + (self._colorMaxShelf.blue() - self._colorMinShelf.blue()) * float(count / len(self._model.get_shelves())))
            shelf.set_color(color)
            self._scene.addItem(shelf)
            self._itemsInScene.append(shelf)
            text = self._scene.addText("S(" + str(shelf.get_id()) + ")", font)
            text.setPos((shelf.get_positionx() - 1.0) * self._w_distance + self._border_size, 
                            (shelf.get_positiony() - 0.8) * self._h_distance + self._border_size)
            self._itemsInScene.append(text)
            count += 1.0
            if robot != None:
                color = QColor(255, 0, 0)
                ellipse = self._scene.addEllipse(shelf.rect().left() + self._objectSize / 4, 
                                                            shelf.rect().top() + self._objectSize / 4, 
                                                            self._objectSize / 2, self._objectSize / 2)
                brush = QBrush(color)
                ellipse.setBrush(brush)
                self._itemsInScene.append(ellipse)

    def moveItem(self, item, move):
        if not (move.x() < 0 or move.y() < 0 or move.x() > self._scene.width() or move.y() > self._scene.height()):
            x = int(move.x() / self._w_distance + 1)
            y = int(move.y() / self._h_distance + 1)

            if not self._model.is_node(x,y):
                self.update()
                return

            if not (x > self._model.getGridSizeX() or y > self._model.getGridSizeY() or x <= 0 or y <= 0):
                if isinstance(item, robot.Robot):
                    item2 = self._model.getRobotInNode(x, y)
                    if item2 != None:
                        item2.set_starting_position(item.get_positionx(), item.get_positiony())
                        item2.set_position(item.get_positionx(), item.get_positiony())
            
                elif isinstance(item, shelf.Shelf):
                    item2 = self._model.getShelfInNode(x, y)
                    if item2 != None:
                        item2.set_starting_position(item.get_positionx(), item.get_positiony())
                        item2.set_position(item.get_positionx(), item.get_positiony())

                elif isinstance(item, pickingStation.PickingStation):
                    item2 = self._model.getPickingStationInNode(x, y)
                    if item2 != None:
                        item2.set_starting_position(item.get_positionx(), item.get_positiony())
                        item2.set_position(item.get_positionx(), item.get_positiony())
        
                item.set_starting_position(x, y)
                item.set_position(x, y)
        self.update()

    def getModel(self):
        return self._model
    
