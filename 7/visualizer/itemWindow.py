from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

class ProductDialog(QWidget):
    def __init__(self):
        super(self.__class__, self).__init__()
        
        self._shelf = None
        self._item_window = None
        self.setWindowTitle("Add product")
        self._id_text = QLineEdit(self)
        self._id_text.setText("product id: ")
        self._id_text.setReadOnly(True)
        self._count_text = QLineEdit(self)
        self._count_text.setText("product count: ")
        self._count_text.setReadOnly(True)
        self._id_textbox = QLineEdit(self)
        self._id_textbox.setText("0")
        self._count_textbox = QLineEdit(self)
        self._count_textbox.setText("0")
        self._ok_button = QPushButton('Ok', self)
        self._cancel_button = QPushButton('Cancel', self)
        self._function = None

        self._ok_button.clicked.connect(self.on_ok)
        self._cancel_button.clicked.connect(self.on_cancel)
        self.setFixedSize(320, 110)
        self._id_text.resize(140, 30)
        self._count_text.resize(140, 30)
        self._id_textbox.resize(140,30)
        self._count_textbox.resize(140,30)
        self._id_text.move(0,0)
        self._count_text.move(0,40)
        self._id_textbox.move(140,0)
        self._count_textbox.move(140,40)
        self._ok_button.move(20,80)
        self._cancel_button.move(140,80)
    
    def set_shelf(self, shelf):
        self._shelf = shelf

    def set_item_window(self, item_window):
        self._item_window = item_window

    def on_ok(self, event):
        self.hide()
        try:
            self._shelf.addItem(int(self._id_textbox.text()), int(self._count_textbox.text()))
        except ValueError:
            print "the product id and the product counts must be integer values"
        self._item_window.update()

    def on_cancel(self, event):
        self.hide()

class ItemWindow(QTreeWidget):
    def __init__(self):                        #init item window
        super(self.__class__, self).__init__()
        self._model = None        

        self.setContextMenuPolicy(Qt.DefaultContextMenu)
        self._menu = QMenu()
        self._menu.setParent(self)
        self._product_dialog = ProductDialog()
        self._product_dialog.set_item_window(self)

    def setModel(self, model):
        self._model = model
        self.setHeaderLabels(["product ID", "count", "removed"])
        self.update()
        if self._model is not None:
            self._model.add_window(self)

    def update(self):
        if self._model == None: 
            return
        expanded_items = []
        for ii in range(0, self.topLevelItemCount()):
            item = self.topLevelItem(ii)
            if item.isExpanded():
                expanded_items.append(ii)

        self.clear()

        for shelf in self._model.get_shelves():
            treeItem = QTreeWidgetItem(["Shelf(" + str(shelf.get_id()) + ")"])
            self.addTopLevelItem(treeItem)
            for item in shelf.getItems():
                temp_item = QTreeWidgetItem([str(item[0]), str(item[1]), str(item[2])])
                treeItem.addChild(temp_item)
        for item in expanded_items:
            self.expandItem(self.topLevelItem(item))

    def show(self):
        self.update()
        return super(self.__class__, self).show()

    def contextMenuEvent(self, event):

        if not self._model.is_drag_enabled(): return 
        item = self.itemAt(event.x(),event.y())
        if item is None: return
        parent = item.parent()

        #find the shelf_index
        shelf_index = 0
        shelf = None
        if parent is None:
            shelf_index = self.indexOfTopLevelItem(item)
        else:
            shelf_index = self.indexOfTopLevelItem(parent)
        
        #get the shelf
        count = 0
        for shelf2 in self._model.get_shelves():
            if count == shelf_index:
                shelf = shelf2
                break
            else:
                count = count + 1

        self._menu.clear()
        if parent is not None:

            #get the product_id
            product_id = 0
            count = 0
            for product in shelf.getItems():
                if count == parent.indexOfChild(item):
                    product_id = product[0]
                    break
            else:
                count = count + 1

            remove_item_action = QAction("remove product", self)
            remove_item_action.setShortcut("Ctrl + R")
            remove_item_action.setStatusTip("Removes a product from the selected shelf")
            remove_item_action.triggered.connect(lambda: self.delete_product(shelf, product_id))  
            self._menu.addAction(remove_item_action)

        add_item_action = QAction("add product", self)
        add_item_action.setShortcut("Ctrl + A")
        add_item_action.setStatusTip("Adds a product to the selected shelf")
        add_item_action.triggered.connect(lambda: self.add_product(shelf))  
        self._menu.addAction(add_item_action)

        self._menu.popup(QPoint(event.x(),event.y()))

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton or event.button() == Qt.MiddleButton:
            self._menu.hide()
        super(self.__class__, self).mousePressEvent(event)

    def add_product(self, shelf):
        self._product_dialog.set_shelf(shelf)
        self._menu.hide()
        self._product_dialog.show()

    def delete_product(self, shelf, product_id):
        shelf.delete_item(product_id) 
        self._menu.hide()
        self.update()
        
