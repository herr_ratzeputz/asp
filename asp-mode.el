;;; asp-mode-el --- Simple major mode for asp logic programs


;;; Commentary:

;; This is a simple syntax highlighting mode for ASP (Answer Set Programming)
;; logical programs. These programs tend to end with *.lp

;;; Code:
(provide 'asp-mode)

(defvar asp-mode-hook  (lambda ()
      (push '(":-" . ?⟵) prettify-symbols-alist)
      (push '(">=" . ?≥) prettify-symbols-alist)
      (push '("<=" . ?≤) prettify-symbols-alist)
      (push '("!=" . ?≠) prettify-symbols-alist)
      (push '("!=" . ?≠) prettify-symbols-alist)
      (push '("not" . ?¬) prettify-symbols-alist)))

;; define tab width
(defvar asp-tab-width 2 "Width of a tab for ASP mode")

;; define custom fonts locked 
(defvar asp-font-lock-defaults
    '((
       ;; display elements
       ("#.*" . font-lock-builtin-face)
       ;; functions
       ("[a-z][a-zA-Z0-9]+[^(,)]" . font-lock-function-name-face)
       ;;("[0-9]+" . font-lock-string-face)
       ("[A-Z0-9_']*" . font-lock-string-face)
       ("_" . font-lock-constant-face)
       ;; ; : , ; { } | + - = ! are all special elements
       ("_\\|\\.\\|!\\|+\\|-\\||\\|:\\|,\\|;\\|{\\|}\\|=" . font-lock-keyword-face)
       )))

;; modify the syntax table
(defvar asp-mode-syntax-table
  (let ((st (make-syntax-table)))
            (modify-syntax-entry ?% "<" st)
            (modify-syntax-entry ?\n ">" st)
            st)
  "Syntax table for ASP mode")

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.lp\\'" . asp-mode))

(define-derived-mode asp-mode fundamental-mode "ASP Mode"
  "ASP mode is major mode for editing logical programs"
  ;; set the default font locks
  (setq font-lock-defaults asp-font-lock-defaults)

  ;; set the tab width
  (when asp-tab-width
    (setq tab-width asp-tab-width))

  :syntax-table asp-mode-syntax-table
  )

