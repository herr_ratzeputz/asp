## ASP - Answer Set Programming

This document is intended to show my way of learning asp.
In order to follow the path you will need (clasp)[http://potassco.sourceforge.net/],
some background in formal logic and a text editor.


The steps are numbered from one to n, where some starts with a lower number
before string a higher number.  
Every program is split into an instance part `*_inst.lp` and 
an encoding or solution description `*.lp`.


* `1` towers of hanoi
  + it introduces the different steps and the basic syntax
* `2` n queens problem
  + a first basic example
* `3` soduko
  + a normal program
* `4` lights puzzle
  + a more complex logic puzzle

### Emacs mode

The file `asp-mode.el` is a basic emacs major mode which provides
syntax highlighting. Just copy it into your `~/.emacs.d/` and add
the path as well as `require('asp-mode)` to your `init.el`.
